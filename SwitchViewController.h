//
//  SwitchViewController.h
//  根控制器， 包含两个内容视图的视图控制器
//
//  Created by  apple on 13-5-16.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YellowViewController;
@class BlueViewController;
@interface SwitchViewController : UIViewController
{
    YellowViewController * yellowViewController;
    BlueViewController * blueViewController;
}

@property (retain, nonatomic) IBOutlet YellowViewController *yellowViewController;
@property (retain, nonatomic) IBOutlet BlueViewController *blueViewController;

-(IBAction)switchViews:(id)sender;
@end
