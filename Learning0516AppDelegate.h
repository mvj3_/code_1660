//
//  Learning0516AppDelegate.h
//  应用程序委托， 声明根控制器
//
//  Created by  apple on 13-5-16.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SwitchViewController;
@interface Learning0516AppDelegate : UIResponder <UIApplicationDelegate>
{
    SwitchViewController *switchViewController;
}
@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) IBOutlet SwitchViewController *switchViewController;
@end
